import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useRoundStore = defineStore("round", () => {
  const counts: number[] = [];
  const count = ref(11);
  const printCound = computed(
    () =>
      function arr() {
        for (let i = count; i.value >= 0; i.value--) {
          counts.push(i.value);
        }
      }
  );

  // function arr() {
  //   for (let i = count; i.value >= 0; i.value--) {
  //     counts.push(i.value);
  //   }
  // }

  return { count, printCound, counts };
});
